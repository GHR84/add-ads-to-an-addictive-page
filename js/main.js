var addHeader = function(h){
    return h;
}

var addImg = function(i){
    return i;
}

var addAd = function(a){
    return a;
}

var renderAd = function(){

    // CAR FOR SALE
    document.querySelector('#ad').innerHTML += addHeader(
        '<h2>Bíll til sölu</h2>'
    
    );
    document.querySelector('#ad').innerHTML += addImg(
        '<img src="bill.jpg">'
    );
    document.querySelector('#ad').innerHTML += addAd(
        '<p>Hér er rosalega flottur bíll til sölu. Hann er keyrður mjög lítið og selst fyrir mjög mikið. Frekari upplýsingar er hægt að fá hjá Sigrúnu í síma 555-5555.</p>'
    );

    //REQUESTING A BIKE

    document.querySelector('#ad').innerHTML += addHeader(
        '<h2>Barnahjól óskast</h2>'
    );
    document.querySelector('#ad').innerHTML += addImg(
        '<img src="bike.jpg">'
    );
    document.querySelector('#ad').innerHTML += addAd(
        '<p>Óska eftir sambærilegu hjóli, verður að vera með hjálpardekkjum. Hugsað fyrir 3+ ára. Er tilbúin til að greiða allt að 5000 kr. Endilega hafið samband í síma 666-6666</p>'
    );

    //Vinur óskast

    document.querySelector('#ad').innerHTML += addHeader(
        '<h2>Vinur óskast</h2>'
    );
    document.querySelector('#ad').innerHTML += addImg(
        '<img src="friend.jpg">'
    );
    document.querySelector('#ad').innerHTML += addAd(
        '<p>Óska eftir vin til að fara í bíó eða bingó af og til.</p>'
    );

}

renderAd();

module.exports = {
    addHeader,
    addImg,
    addAd,
    renderAd
};